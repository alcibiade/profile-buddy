#!/bin/bash

echo "Removing stopped containers"
docker ps -q -a | xargs -r docker rm

echo "Removing volumes"
docker volume ls -q | xargs -r docker volume rm

echo "Removing untagged images"
docker images | grep '^<none>' | awk '{ print $3 }' | xargs -r docker rmi
