[![Build Status][travis-image]][travis-url]

## Profile Buddy

A document manager focusing on HR requirements.

## Requirements

This project requires Docker and docker-compose for setup of the complete environment.

## Startup

The project folder contains a docker-compose.yml configuration that will bootstrap all the components:

```
yk@triton:~/Documents/profile-buddy$ docker-compose up -d
Starting profilebuddy_search_1
Starting profilebuddy_database_1
Starting profilebuddy_webfs_1
Starting profilebuddy_crawler_1
Starting profilebuddy_kibana_1

```

From there you can access all the components directly:
* ElasticSearch API is accessible from http://localhost:9200/
* Kibana frontend user interface is http://localhost:5601/ and the crawler index name is 'crawl' and does not contain any time series.
* PostgreSQL is open on jdbc:postgresql://localhost:5432/hr1 with user 'hr' and password 'hr'
* A web user interface for document management (extplorer): http://localhost:8088/wfm/ (user: 'data' password: 'data')



[travis-image]: https://travis-ci.org/alcibiade/profile-buddy.svg?branch=master
[travis-url]: https://travis-ci.org/alcibiade/profile-buddy
