#!/bin/bash

echo "Stopping containers"
docker ps -q | xargs -r docker stop

echo "Removing containers"
docker ps -q -a | xargs -r docker rm

echo "Removing volumes"
docker volume ls -q | xargs -r docker volume rm

echo "Removing images"
docker images -q | xargs -r docker rmi

echo "Removing networks"
docker network ls -q | xargs docker network rm 2>/dev/null
