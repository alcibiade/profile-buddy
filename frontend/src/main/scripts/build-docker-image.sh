#!/bin/bash

SCRIPT_BASE=$(readlink -f $(dirname $0))
PROJECT_BASE=$(readlink -f $(dirname $0)/../../..)


## Local build
## cd ${PROJECT_BASE}
## ./mvnw -Pprod -Pswagger package

# Container build

cd ${SCRIPT_BASE}
docker build -t pb-build .
docker run -it --name pb-build-01 -v $PWD/../../../target/:/target pb-build
docker rm pb-build-01
docker rmi pb-build

cd ${PROJECT_BASE}
docker build -t profile-buddy .
