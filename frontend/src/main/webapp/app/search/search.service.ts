import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs';

@Injectable()
export class SearchService {

    private apiEndPoint = '/api/search';

    constructor(private http: Http) {
    }

    search(term: String): Observable<Response> {
        console.log('Search for term:', term);
        return this.http.get(`${this.apiEndPoint}?term=${term}`);
    }
}
