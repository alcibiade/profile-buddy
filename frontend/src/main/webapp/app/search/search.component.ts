import {Component, OnInit} from '@angular/core';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiLanguageService} from 'ng-jhipster';
import {SearchService} from './search.service';

@Component({
    selector: 'jhi-search',
    templateUrl: './search.component.html',
    styleUrls: [
        'search.scss'
    ]

})
export class SearchComponent implements OnInit {
    modalRef: NgbModalRef;
    searchResults: SearchResult[];
    loading: Boolean;

    constructor(private jhiLanguageService: JhiLanguageService, private searchService: SearchService) {
        this.jhiLanguageService.setLocations(['search']);
        this.searchResults = [];
        this.loading = false;
    }

    ngOnInit() {
    }

    change(evt) {
        console.log(evt);
        this.loading = true;
        this.searchService.search(evt.srcElement.value).forEach(r => {
            let response = r.json();
            console.log('Response: ', response);
            this.searchResults = response.documents;
            this.loading = false;
        });
    }
}

class SearchResult {
    name: string;
    size: number;
    type: string;

    constructor(name: string, size: number, type: string) {
        this.name = name;
        this.size = size;
        this.type = type;
    }
}
