import {Route} from '@angular/router';
import {UserRouteAccessService} from '../shared';
import {SearchComponent} from './';

export const SEARCH_ROUTE: Route = {
    path: 'search',
    component: SearchComponent,
    data: {
        authorities: [],
        pageTitle: 'profileBuddyApp.search.title'
    },
    canActivate: [UserRouteAccessService]
};
