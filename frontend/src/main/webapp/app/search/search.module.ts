import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {ProfileBuddySharedModule} from '../shared';
import {SEARCH_ROUTE} from './search.route';
import {SearchComponent} from './search.component';
import {SearchService} from './search.service';


@NgModule({
    imports: [
        ProfileBuddySharedModule,
        RouterModule.forRoot([SEARCH_ROUTE], {useHash: true})
    ],
    declarations: [
        SearchComponent,
    ],
    entryComponents: [],
    providers: [
        SearchService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProfileBuddySearchModule {
}
