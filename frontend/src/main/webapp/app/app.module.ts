import './vendor.ts';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {Ng2Webstorage} from 'ng2-webstorage';
import {ProfileBuddySharedModule, UserRouteAccessService} from './shared';
import {ProfileBuddyHomeModule} from './home/home.module';
import {ProfileBuddyAdminModule} from './admin/admin.module';
import {ProfileBuddyAccountModule} from './account/account.module';
import {ProfileBuddyEntityModule} from './entities/entity.module';
import {
    ActiveMenuDirective,
    ErrorComponent,
    FooterComponent,
    JhiMainComponent,
    LayoutRoutingModule,
    NavbarComponent,
    PageRibbonComponent,
    ProfileService
} from './layouts';
import {customHttpProvider} from './blocks/interceptor/http.provider';
import {PaginationConfig} from './blocks/config/uib-pagination.config';
import {ProfileBuddyUploadModule} from './upload/upload.module';
import {ProfileBuddySearchModule} from './search/search.module';


@NgModule({
    imports: [
        BrowserModule,
        LayoutRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        ProfileBuddySharedModule,
        ProfileBuddyHomeModule,
        ProfileBuddyUploadModule,
        ProfileBuddySearchModule,
        ProfileBuddyAdminModule,
        ProfileBuddyAccountModule,
        ProfileBuddyEntityModule
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent
    ],
    providers: [
        ProfileService,
        { provide: Window, useValue: window },
        { provide: Document, useValue: document },
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService
    ],
    bootstrap: [ JhiMainComponent ]
})
export class ProfileBuddyAppModule {}
