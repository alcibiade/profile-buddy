import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {AlertService, EventManager, JhiLanguageService} from 'ng-jhipster';

import {ProfileMySuffix} from './profile-my-suffix.model';
import {ProfileMySuffixPopupService} from './profile-my-suffix-popup.service';
import {ProfileMySuffixService} from './profile-my-suffix.service';
import {ProfileDocumentMySuffix, ProfileDocumentMySuffixService} from '../profile-document';
@Component({
    selector: 'jhi-profile-my-suffix-dialog',
    templateUrl: './profile-my-suffix-dialog.component.html'
})
export class ProfileMySuffixDialogComponent implements OnInit {

    profile: ProfileMySuffix;
    authorities: any[];
    isSaving: boolean;

    profiledocuments: ProfileDocumentMySuffix[];
    constructor(
        public activeModal: NgbActiveModal,
        private jhiLanguageService: JhiLanguageService,
        private alertService: AlertService,
        private profileService: ProfileMySuffixService,
        private profileDocumentService: ProfileDocumentMySuffixService,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['profile']);
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.profileDocumentService.query().subscribe(
            (res: Response) => {
                this.profiledocuments = res.json();
            }, (res: Response) => this.onError(res.json()));
    }
    clear () {
        this.activeModal.dismiss('cancel');
    }

    save () {
        this.isSaving = true;
        if (this.profile.id !== undefined) {
            this.profileService.update(this.profile)
                .subscribe((res: ProfileMySuffix) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        } else {
            this.profileService.create(this.profile)
                .subscribe((res: ProfileMySuffix) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        }
    }

    private onSaveSuccess (result: ProfileMySuffix) {
        this.eventManager.broadcast({ name: 'profileListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError (error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onError (error) {
        this.alertService.error(error.message, null, null);
    }

    trackProfileDocumentById(index: number, item: ProfileDocumentMySuffix) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-profile-my-suffix-popup',
    template: ''
})
export class ProfileMySuffixPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private profilePopupService: ProfileMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            if ( params['id'] ) {
                this.modalRef = this.profilePopupService
                    .open(ProfileMySuffixDialogComponent, params['id']);
            } else {
                this.modalRef = this.profilePopupService
                    .open(ProfileMySuffixDialogComponent);
            }

        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
