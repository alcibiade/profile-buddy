import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {ProfileBuddySharedModule} from '../../shared';

import {
    ProfileMySuffixComponent,
    ProfileMySuffixDeleteDialogComponent,
    ProfileMySuffixDeletePopupComponent,
    ProfileMySuffixDetailComponent,
    ProfileMySuffixDialogComponent,
    ProfileMySuffixPopupComponent,
    ProfileMySuffixPopupService,
    ProfileMySuffixService,
    profilePopupRoute,
    profileRoute
} from './';

let ENTITY_STATES = [
    ...profileRoute,
    ...profilePopupRoute,
];

@NgModule({
    imports: [
        ProfileBuddySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ProfileMySuffixComponent,
        ProfileMySuffixDetailComponent,
        ProfileMySuffixDialogComponent,
        ProfileMySuffixDeleteDialogComponent,
        ProfileMySuffixPopupComponent,
        ProfileMySuffixDeletePopupComponent,
    ],
    entryComponents: [
        ProfileMySuffixComponent,
        ProfileMySuffixDialogComponent,
        ProfileMySuffixPopupComponent,
        ProfileMySuffixDeleteDialogComponent,
        ProfileMySuffixDeletePopupComponent,
    ],
    providers: [
        ProfileMySuffixService,
        ProfileMySuffixPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProfileBuddyProfileMySuffixModule {}
