import {Routes} from '@angular/router';

import {ProfileMySuffixComponent} from './profile-my-suffix.component';
import {ProfileMySuffixDetailComponent} from './profile-my-suffix-detail.component';
import {ProfileMySuffixPopupComponent} from './profile-my-suffix-dialog.component';
import {ProfileMySuffixDeletePopupComponent} from './profile-my-suffix-delete-dialog.component';


export const profileRoute: Routes = [
  {
    path: 'profile-my-suffix',
    component: ProfileMySuffixComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'profileBuddyApp.profile.home.title'
    }
  }, {
    path: 'profile-my-suffix/:id',
    component: ProfileMySuffixDetailComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'profileBuddyApp.profile.home.title'
    }
  }
];

export const profilePopupRoute: Routes = [
  {
    path: 'profile-my-suffix-new',
    component: ProfileMySuffixPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'profileBuddyApp.profile.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'profile-my-suffix/:id/edit',
    component: ProfileMySuffixPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'profileBuddyApp.profile.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'profile-my-suffix/:id/delete',
    component: ProfileMySuffixDeletePopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'profileBuddyApp.profile.home.title'
    },
    outlet: 'popup'
  }
];
