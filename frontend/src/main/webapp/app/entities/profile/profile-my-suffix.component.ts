import {Component, OnDestroy, OnInit} from '@angular/core';
import {Response} from '@angular/http';
import {Subscription} from 'rxjs/Rx';
import {AlertService, EventManager, JhiLanguageService} from 'ng-jhipster';

import {ProfileMySuffix} from './profile-my-suffix.model';
import {ProfileMySuffixService} from './profile-my-suffix.service';
import {Principal} from '../../shared';

@Component({
    selector: 'jhi-profile-my-suffix',
    templateUrl: './profile-my-suffix.component.html'
})
export class ProfileMySuffixComponent implements OnInit, OnDestroy {
profiles: ProfileMySuffix[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private profileService: ProfileMySuffixService,
        private alertService: AlertService,
        private eventManager: EventManager,
        private principal: Principal
    ) {
        this.jhiLanguageService.setLocations(['profile']);
    }

    loadAll() {
        this.profileService.query().subscribe(
            (res: Response) => {
                this.profiles = res.json();
            },
            (res: Response) => this.onError(res.json())
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInProfiles();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId (index: number, item: ProfileMySuffix) {
        return item.id;
    }



    registerChangeInProfiles() {
        this.eventSubscriber = this.eventManager.subscribe('profileListModification', (response) => this.loadAll());
    }


    private onError (error) {
        this.alertService.error(error.message, null, null);
    }
}
