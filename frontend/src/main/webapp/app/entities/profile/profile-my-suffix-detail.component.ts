import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {JhiLanguageService} from 'ng-jhipster';
import {ProfileMySuffix} from './profile-my-suffix.model';
import {ProfileMySuffixService} from './profile-my-suffix.service';

@Component({
    selector: 'jhi-profile-my-suffix-detail',
    templateUrl: './profile-my-suffix-detail.component.html'
})
export class ProfileMySuffixDetailComponent implements OnInit, OnDestroy {

    profile: ProfileMySuffix;
    private subscription: any;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private profileService: ProfileMySuffixService,
        private route: ActivatedRoute
    ) {
        this.jhiLanguageService.setLocations(['profile']);
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe(params => {
            this.load(params['id']);
        });
    }

    load (id) {
        this.profileService.find(id).subscribe(profile => {
            this.profile = profile;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
