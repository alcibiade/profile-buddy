import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {EventManager, JhiLanguageService} from 'ng-jhipster';

import {ProfileMySuffix} from './profile-my-suffix.model';
import {ProfileMySuffixPopupService} from './profile-my-suffix-popup.service';
import {ProfileMySuffixService} from './profile-my-suffix.service';

@Component({
    selector: 'jhi-profile-my-suffix-delete-dialog',
    templateUrl: './profile-my-suffix-delete-dialog.component.html'
})
export class ProfileMySuffixDeleteDialogComponent {

    profile: ProfileMySuffix;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private profileService: ProfileMySuffixService,
        public activeModal: NgbActiveModal,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['profile']);
    }

    clear () {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete (id: number) {
        this.profileService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'profileListModification',
                content: 'Deleted an profile'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-profile-my-suffix-delete-popup',
    template: ''
})
export class ProfileMySuffixDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private profilePopupService: ProfileMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.modalRef = this.profilePopupService
                .open(ProfileMySuffixDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
