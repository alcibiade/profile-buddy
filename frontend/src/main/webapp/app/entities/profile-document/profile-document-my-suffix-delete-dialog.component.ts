import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {EventManager, JhiLanguageService} from 'ng-jhipster';

import {ProfileDocumentMySuffix} from './profile-document-my-suffix.model';
import {ProfileDocumentMySuffixPopupService} from './profile-document-my-suffix-popup.service';
import {ProfileDocumentMySuffixService} from './profile-document-my-suffix.service';

@Component({
    selector: 'jhi-profile-document-my-suffix-delete-dialog',
    templateUrl: './profile-document-my-suffix-delete-dialog.component.html'
})
export class ProfileDocumentMySuffixDeleteDialogComponent {

    profileDocument: ProfileDocumentMySuffix;

    constructor(private jhiLanguageService: JhiLanguageService,
                private profileDocumentService: ProfileDocumentMySuffixService,
                public activeModal: NgbActiveModal,
                private eventManager: EventManager) {
        this.jhiLanguageService.setLocations(['profileDocument']);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.profileDocumentService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'profileDocumentListModification',
                content: 'Deleted an profileDocument'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-profile-document-my-suffix-delete-popup',
    template: ''
})
export class ProfileDocumentMySuffixDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(private route: ActivatedRoute,
                private profileDocumentPopupService: ProfileDocumentMySuffixPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.modalRef = this.profileDocumentPopupService
                .open(ProfileDocumentMySuffixDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
