export class ProfileDocumentMySuffix {
    constructor(public id?: number,
                public path?: string,
                public name?: string,
                public profileId?: number) {
    }
}
