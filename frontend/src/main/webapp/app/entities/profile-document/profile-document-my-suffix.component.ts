import {Component, OnDestroy, OnInit} from '@angular/core';
import {Response} from '@angular/http';
import {Subscription} from 'rxjs/Rx';
import {AlertService, EventManager, JhiLanguageService} from 'ng-jhipster';

import {ProfileDocumentMySuffix} from './profile-document-my-suffix.model';
import {ProfileDocumentMySuffixService} from './profile-document-my-suffix.service';
import {Principal} from '../../shared';

@Component({
    selector: 'jhi-profile-document-my-suffix',
    templateUrl: './profile-document-my-suffix.component.html'
})
export class ProfileDocumentMySuffixComponent implements OnInit, OnDestroy {
    profileDocuments: ProfileDocumentMySuffix[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(private jhiLanguageService: JhiLanguageService,
                private profileDocumentService: ProfileDocumentMySuffixService,
                private alertService: AlertService,
                private eventManager: EventManager,
                private principal: Principal) {
        this.jhiLanguageService.setLocations(['profileDocument']);
    }

    loadAll() {
        this.profileDocumentService.query().subscribe(
            (res: Response) => {
                this.profileDocuments = res.json();
            },
            (res: Response) => this.onError(res.json())
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInProfileDocuments();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ProfileDocumentMySuffix) {
        return item.id;
    }


    registerChangeInProfileDocuments() {
        this.eventSubscriber = this.eventManager.subscribe('profileDocumentListModification', (response) => this.loadAll());
    }


    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
