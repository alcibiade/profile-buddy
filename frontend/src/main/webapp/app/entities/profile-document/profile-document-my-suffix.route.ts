import {Routes} from '@angular/router';

import {ProfileDocumentMySuffixComponent} from './profile-document-my-suffix.component';
import {ProfileDocumentMySuffixDetailComponent} from './profile-document-my-suffix-detail.component';
import {ProfileDocumentMySuffixPopupComponent} from './profile-document-my-suffix-dialog.component';
import {ProfileDocumentMySuffixDeletePopupComponent} from './profile-document-my-suffix-delete-dialog.component';


export const profileDocumentRoute: Routes = [
    {
        path: 'profile-document-my-suffix',
        component: ProfileDocumentMySuffixComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'profileBuddyApp.profileDocument.home.title'
        }
    }, {
        path: 'profile-document-my-suffix/:id',
        component: ProfileDocumentMySuffixDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'profileBuddyApp.profileDocument.home.title'
        }
    }
];

export const profileDocumentPopupRoute: Routes = [
    {
        path: 'profile-document-my-suffix-new',
        component: ProfileDocumentMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'profileBuddyApp.profileDocument.home.title'
        },
        outlet: 'popup'
    },
    {
        path: 'profile-document-my-suffix/:id/edit',
        component: ProfileDocumentMySuffixPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'profileBuddyApp.profileDocument.home.title'
        },
        outlet: 'popup'
    },
    {
        path: 'profile-document-my-suffix/:id/delete',
        component: ProfileDocumentMySuffixDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'profileBuddyApp.profileDocument.home.title'
        },
        outlet: 'popup'
    }
];
