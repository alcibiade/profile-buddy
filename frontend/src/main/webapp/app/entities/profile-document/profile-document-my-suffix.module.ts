import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {ProfileBuddySharedModule} from '../../shared';

import {
    ProfileDocumentMySuffixComponent,
    ProfileDocumentMySuffixDeleteDialogComponent,
    ProfileDocumentMySuffixDeletePopupComponent,
    ProfileDocumentMySuffixDetailComponent,
    ProfileDocumentMySuffixDialogComponent,
    ProfileDocumentMySuffixPopupComponent,
    ProfileDocumentMySuffixPopupService,
    ProfileDocumentMySuffixService,
    profileDocumentPopupRoute,
    profileDocumentRoute
} from './';

let ENTITY_STATES = [
    ...profileDocumentRoute,
    ...profileDocumentPopupRoute,
];

@NgModule({
    imports: [
        ProfileBuddySharedModule,
        RouterModule.forRoot(ENTITY_STATES, {useHash: true})
    ],
    declarations: [
        ProfileDocumentMySuffixComponent,
        ProfileDocumentMySuffixDetailComponent,
        ProfileDocumentMySuffixDialogComponent,
        ProfileDocumentMySuffixDeleteDialogComponent,
        ProfileDocumentMySuffixPopupComponent,
        ProfileDocumentMySuffixDeletePopupComponent,
    ],
    entryComponents: [
        ProfileDocumentMySuffixComponent,
        ProfileDocumentMySuffixDialogComponent,
        ProfileDocumentMySuffixPopupComponent,
        ProfileDocumentMySuffixDeleteDialogComponent,
        ProfileDocumentMySuffixDeletePopupComponent,
    ],
    providers: [
        ProfileDocumentMySuffixService,
        ProfileDocumentMySuffixPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProfileBuddyProfileDocumentMySuffixModule {
}
