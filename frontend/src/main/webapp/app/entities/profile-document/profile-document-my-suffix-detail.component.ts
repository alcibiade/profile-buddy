import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {JhiLanguageService} from 'ng-jhipster';
import {ProfileDocumentMySuffix} from './profile-document-my-suffix.model';
import {ProfileDocumentMySuffixService} from './profile-document-my-suffix.service';

@Component({
    selector: 'jhi-profile-document-my-suffix-detail',
    templateUrl: './profile-document-my-suffix-detail.component.html'
})
export class ProfileDocumentMySuffixDetailComponent implements OnInit, OnDestroy {

    profileDocument: ProfileDocumentMySuffix;
    private subscription: any;

    constructor(private jhiLanguageService: JhiLanguageService,
                private profileDocumentService: ProfileDocumentMySuffixService,
                private route: ActivatedRoute) {
        this.jhiLanguageService.setLocations(['profileDocument']);
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe(params => {
            this.load(params['id']);
        });
    }

    load(id) {
        this.profileDocumentService.find(id).subscribe(profileDocument => {
            this.profileDocument = profileDocument;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
