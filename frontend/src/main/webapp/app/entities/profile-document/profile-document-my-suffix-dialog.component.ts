import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {AlertService, EventManager, JhiLanguageService} from 'ng-jhipster';

import {ProfileDocumentMySuffix} from './profile-document-my-suffix.model';
import {ProfileDocumentMySuffixPopupService} from './profile-document-my-suffix-popup.service';
import {ProfileDocumentMySuffixService} from './profile-document-my-suffix.service';
import {ProfileMySuffix, ProfileMySuffixService} from '../profile';
@Component({
    selector: 'jhi-profile-document-my-suffix-dialog',
    templateUrl: './profile-document-my-suffix-dialog.component.html'
})
export class ProfileDocumentMySuffixDialogComponent implements OnInit {

    profileDocument: ProfileDocumentMySuffix;
    authorities: any[];
    isSaving: boolean;

    profiles: ProfileMySuffix[];

    constructor(public activeModal: NgbActiveModal,
                private jhiLanguageService: JhiLanguageService,
                private alertService: AlertService,
                private profileDocumentService: ProfileDocumentMySuffixService,
                private profileService: ProfileMySuffixService,
                private eventManager: EventManager) {
        this.jhiLanguageService.setLocations(['profileDocument']);
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.profileService.query().subscribe(
            (res: Response) => {
                this.profiles = res.json();
            }, (res: Response) => this.onError(res.json()));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.profileDocument.id !== undefined) {
            this.profileDocumentService.update(this.profileDocument)
                .subscribe((res: ProfileDocumentMySuffix) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        } else {
            this.profileDocumentService.create(this.profileDocument)
                .subscribe((res: ProfileDocumentMySuffix) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        }
    }

    private onSaveSuccess(result: ProfileDocumentMySuffix) {
        this.eventManager.broadcast({name: 'profileDocumentListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackProfileById(index: number, item: ProfileMySuffix) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-profile-document-my-suffix-popup',
    template: ''
})
export class ProfileDocumentMySuffixPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(private route: ActivatedRoute,
                private profileDocumentPopupService: ProfileDocumentMySuffixPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            if (params['id']) {
                this.modalRef = this.profileDocumentPopupService
                    .open(ProfileDocumentMySuffixDialogComponent, params['id']);
            } else {
                this.modalRef = this.profileDocumentPopupService
                    .open(ProfileDocumentMySuffixDialogComponent);
            }

        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
