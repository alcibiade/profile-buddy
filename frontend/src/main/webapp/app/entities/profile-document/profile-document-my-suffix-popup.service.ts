import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ProfileDocumentMySuffix} from './profile-document-my-suffix.model';
import {ProfileDocumentMySuffixService} from './profile-document-my-suffix.service';
@Injectable()
export class ProfileDocumentMySuffixPopupService {
    private isOpen = false;

    constructor(private modalService: NgbModal,
                private router: Router,
                private profileDocumentService: ProfileDocumentMySuffixService) {
    }

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.profileDocumentService.find(id).subscribe(profileDocument => {
                this.profileDocumentModalRef(component, profileDocument);
            });
        } else {
            return this.profileDocumentModalRef(component, new ProfileDocumentMySuffix());
        }
    }

    profileDocumentModalRef(component: Component, profileDocument: ProfileDocumentMySuffix): NgbModalRef {
        let modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.profileDocument = profileDocument;
        modalRef.result.then(result => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true});
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true});
            this.isOpen = false;
        });
        return modalRef;
    }
}
