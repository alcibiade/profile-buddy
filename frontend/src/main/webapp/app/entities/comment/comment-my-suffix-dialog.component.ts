import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {AlertService, EventManager, JhiLanguageService} from 'ng-jhipster';

import {CommentMySuffix} from './comment-my-suffix.model';
import {CommentMySuffixPopupService} from './comment-my-suffix-popup.service';
import {CommentMySuffixService} from './comment-my-suffix.service';
import {ProfileMySuffix, ProfileMySuffixService} from '../profile';
@Component({
    selector: 'jhi-comment-my-suffix-dialog',
    templateUrl: './comment-my-suffix-dialog.component.html'
})
export class CommentMySuffixDialogComponent implements OnInit {

    comment: CommentMySuffix;
    authorities: any[];
    isSaving: boolean;

    profiles: ProfileMySuffix[];
    constructor(
        public activeModal: NgbActiveModal,
        private jhiLanguageService: JhiLanguageService,
        private alertService: AlertService,
        private commentService: CommentMySuffixService,
        private profileService: ProfileMySuffixService,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['comment']);
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.profileService.query().subscribe(
            (res: Response) => { this.profiles = res.json(); }, (res: Response) => this.onError(res.json()));
    }
    clear () {
        this.activeModal.dismiss('cancel');
    }

    save () {
        this.isSaving = true;
        if (this.comment.id !== undefined) {
            this.commentService.update(this.comment)
                .subscribe((res: CommentMySuffix) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        } else {
            this.commentService.create(this.comment)
                .subscribe((res: CommentMySuffix) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        }
    }

    private onSaveSuccess (result: CommentMySuffix) {
        this.eventManager.broadcast({ name: 'commentListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError (error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onError (error) {
        this.alertService.error(error.message, null, null);
    }

    trackProfileById(index: number, item: ProfileMySuffix) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-comment-my-suffix-popup',
    template: ''
})
export class CommentMySuffixPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private commentPopupService: CommentMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            if ( params['id'] ) {
                this.modalRef = this.commentPopupService
                    .open(CommentMySuffixDialogComponent, params['id']);
            } else {
                this.modalRef = this.commentPopupService
                    .open(CommentMySuffixDialogComponent);
            }

        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
