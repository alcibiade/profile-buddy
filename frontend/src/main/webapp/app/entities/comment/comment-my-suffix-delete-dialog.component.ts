import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {EventManager, JhiLanguageService} from 'ng-jhipster';

import {CommentMySuffix} from './comment-my-suffix.model';
import {CommentMySuffixPopupService} from './comment-my-suffix-popup.service';
import {CommentMySuffixService} from './comment-my-suffix.service';

@Component({
    selector: 'jhi-comment-my-suffix-delete-dialog',
    templateUrl: './comment-my-suffix-delete-dialog.component.html'
})
export class CommentMySuffixDeleteDialogComponent {

    comment: CommentMySuffix;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private commentService: CommentMySuffixService,
        public activeModal: NgbActiveModal,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['comment']);
    }

    clear () {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete (id: number) {
        this.commentService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'commentListModification',
                content: 'Deleted an comment'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-comment-my-suffix-delete-popup',
    template: ''
})
export class CommentMySuffixDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private commentPopupService: CommentMySuffixPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.modalRef = this.commentPopupService
                .open(CommentMySuffixDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
