import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {JhiLanguageService} from 'ng-jhipster';
import {CommentMySuffix} from './comment-my-suffix.model';
import {CommentMySuffixService} from './comment-my-suffix.service';

@Component({
    selector: 'jhi-comment-my-suffix-detail',
    templateUrl: './comment-my-suffix-detail.component.html'
})
export class CommentMySuffixDetailComponent implements OnInit, OnDestroy {

    comment: CommentMySuffix;
    private subscription: any;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private commentService: CommentMySuffixService,
        private route: ActivatedRoute
    ) {
        this.jhiLanguageService.setLocations(['comment']);
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe(params => {
            this.load(params['id']);
        });
    }

    load (id) {
        this.commentService.find(id).subscribe(comment => {
            this.comment = comment;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
