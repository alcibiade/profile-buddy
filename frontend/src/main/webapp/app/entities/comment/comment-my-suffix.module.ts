import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {ProfileBuddySharedModule} from '../../shared';

import {
    CommentMySuffixComponent,
    CommentMySuffixDeleteDialogComponent,
    CommentMySuffixDeletePopupComponent,
    CommentMySuffixDetailComponent,
    CommentMySuffixDialogComponent,
    CommentMySuffixPopupComponent,
    CommentMySuffixPopupService,
    CommentMySuffixService,
    commentPopupRoute,
    commentRoute
} from './';

let ENTITY_STATES = [
    ...commentRoute,
    ...commentPopupRoute,
];

@NgModule({
    imports: [
        ProfileBuddySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CommentMySuffixComponent,
        CommentMySuffixDetailComponent,
        CommentMySuffixDialogComponent,
        CommentMySuffixDeleteDialogComponent,
        CommentMySuffixPopupComponent,
        CommentMySuffixDeletePopupComponent,
    ],
    entryComponents: [
        CommentMySuffixComponent,
        CommentMySuffixDialogComponent,
        CommentMySuffixPopupComponent,
        CommentMySuffixDeleteDialogComponent,
        CommentMySuffixDeletePopupComponent,
    ],
    providers: [
        CommentMySuffixService,
        CommentMySuffixPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProfileBuddyCommentMySuffixModule {}
