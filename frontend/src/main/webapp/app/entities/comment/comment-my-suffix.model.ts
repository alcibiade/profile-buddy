export class CommentMySuffix {
    constructor(
        public id?: number,
        public markdown?: string,
        public profileId?: number,
    ) {
    }
}
