import {Component, OnDestroy, OnInit} from '@angular/core';
import {Response} from '@angular/http';
import {Subscription} from 'rxjs/Rx';
import {AlertService, EventManager, JhiLanguageService} from 'ng-jhipster';

import {CommentMySuffix} from './comment-my-suffix.model';
import {CommentMySuffixService} from './comment-my-suffix.service';
import {Principal} from '../../shared';

@Component({
    selector: 'jhi-comment-my-suffix',
    templateUrl: './comment-my-suffix.component.html'
})
export class CommentMySuffixComponent implements OnInit, OnDestroy {
comments: CommentMySuffix[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private commentService: CommentMySuffixService,
        private alertService: AlertService,
        private eventManager: EventManager,
        private principal: Principal
    ) {
        this.jhiLanguageService.setLocations(['comment']);
    }

    loadAll() {
        this.commentService.query().subscribe(
            (res: Response) => {
                this.comments = res.json();
            },
            (res: Response) => this.onError(res.json())
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInComments();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId (index: number, item: CommentMySuffix) {
        return item.id;
    }



    registerChangeInComments() {
        this.eventSubscriber = this.eventManager.subscribe('commentListModification', (response) => this.loadAll());
    }


    private onError (error) {
        this.alertService.error(error.message, null, null);
    }
}
