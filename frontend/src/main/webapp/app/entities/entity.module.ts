import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {ProfileBuddyCommentMySuffixModule} from './comment/comment-my-suffix.module';
import {ProfileBuddyProfileMySuffixModule} from './profile/profile-my-suffix.module';
import {ProfileBuddyProfileDocumentMySuffixModule} from './profile-document/profile-document-my-suffix.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        ProfileBuddyCommentMySuffixModule,
        ProfileBuddyProfileMySuffixModule,
        ProfileBuddyProfileDocumentMySuffixModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProfileBuddyEntityModule {}
