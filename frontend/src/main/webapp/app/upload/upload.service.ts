import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs';

@Injectable()
export class UploadService {

    private apiEndPoint = '/api/upload';

    constructor(private http: Http) {
    }

    upload(file: File): Observable<Response> {
        console.log('Uploading file', file);
        let formData: FormData = new FormData();
        formData.append('documentFile', file);
        return this.http.post(`${this.apiEndPoint}`, formData);
    }
}
