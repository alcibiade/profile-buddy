import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {ProfileBuddySharedModule} from '../shared';
import {UPLOAD_ROUTE, UploadComponent} from './';
import {UploadService} from './upload.service';


@NgModule({
    imports: [
        ProfileBuddySharedModule,
        RouterModule.forRoot([ UPLOAD_ROUTE ], { useHash: true })
    ],
    declarations: [
        UploadComponent,
    ],
    entryComponents: [
    ],
    providers: [
        UploadService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProfileBuddyUploadModule {}
