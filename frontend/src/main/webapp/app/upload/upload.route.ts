import {Route} from '@angular/router';
import {UserRouteAccessService} from '../shared';
import {UploadComponent} from './';

export const UPLOAD_ROUTE: Route = {
    path: 'upload',
    component: UploadComponent,
    data: {
        authorities: [],
        pageTitle: 'profileBuddyApp.upload.title'
    },
    canActivate: [UserRouteAccessService]
};
