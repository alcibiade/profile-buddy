import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiLanguageService} from 'ng-jhipster';
import {UploadService} from './upload.service';

@Component({
    selector: 'jhi-upload',
    templateUrl: './upload.component.html',
    styleUrls: [
        'upload.scss'
    ]

})
export class UploadComponent implements OnInit {
    modalRef: NgbModalRef;
    @ViewChild('fileInput') fileInput: ElementRef;
    hovering: Boolean;
    uploadedFiles: UploadedFile[];

    constructor(private jhiLanguageService: JhiLanguageService,
                private uploadService: UploadService) {
        this.jhiLanguageService.setLocations(['upload']);
    }

    ngOnInit() {
        this.hovering = false;
        this.uploadedFiles = [];
    }

    isHovering() {
        return this.hovering;
    }

    dragover(event: DragEvent) {
        event.preventDefault();
        event.dataTransfer.dropEffect = 'copy';
        return false;
    }

    dragenter() {
        this.hovering = true;
        return false;
    }

    dragleave() {
        this.hovering = false;
        return false;
    }

    dragend() {
        return false;
    }

    change(evt) {
        this.updateFileList(evt.target.files);
        return false;
    }

    drop(evt: DragEvent) {
        let files = evt.dataTransfer.files;
        this.hovering = false;
        evt.stopPropagation();
        evt.preventDefault();
        this.updateFileList(files);
        return false;
    }

    private updateFileList(files) {
        for (let i = 0; i < files.length; i++) {
            let f = files[i];
            let uf = new UploadedFile(f.name, f.size, f.type);
            this.uploadedFiles.push(uf);
            this.uploadService.upload(f).forEach(r => console.log('File uploaded', r));
        }
    }

    click() {
        this.fileInput.nativeElement.click();
    }
}

class UploadedFile {
    name: string;
    size: number;
    type: string;

    constructor(name: string, size: number, type: string) {
        this.name = name;
        this.size = size;
        this.type = type;
    }
}
