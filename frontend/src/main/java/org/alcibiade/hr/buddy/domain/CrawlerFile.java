package org.alcibiade.hr.buddy.domain;

import java.util.Date;

/**
 * File related document metadata.
 */
public class CrawlerFile {

    private String filename;
    private String content_type;
    private long filesize;
    private Date last_modified;

    public CrawlerFile() {
    }

    public CrawlerFile(String filename, String content_type, long filesize, Date last_modified) {
        this.filename = filename;
        this.content_type = content_type;
        this.filesize = filesize;
        this.last_modified = last_modified;
    }

    public String getFilename() {
        return filename;
    }

    public String getContent_type() {
        return content_type;
    }

    public long getFilesize() {
        return filesize;
    }

    public Date getLast_modified() {
        return last_modified;
    }
}
