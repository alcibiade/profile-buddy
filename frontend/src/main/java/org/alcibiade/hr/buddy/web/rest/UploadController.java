package org.alcibiade.hr.buddy.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.alcibiade.hr.buddy.config.ApplicationProperties;
import org.alcibiade.hr.buddy.service.ProfileDocumentService;
import org.alcibiade.hr.buddy.service.dto.ProfileDocumentDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * API for document upload.
 */
@RestController
@RequestMapping("/api")
public class UploadController {
    private final Logger log = LoggerFactory.getLogger(UploadController.class);
    private final ApplicationProperties applicationProperties;

    private ProfileDocumentService profileDocumentService;

    @Autowired
    public UploadController(ApplicationProperties applicationProperties, ProfileDocumentService profileDocumentService) {
        this.applicationProperties = applicationProperties;
        this.profileDocumentService = profileDocumentService;
    }

    @PostMapping("/upload")
    @Timed
    public ResponseEntity<String> singleFileUpload(@RequestParam("documentFile") MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            return new ResponseEntity<>("Empty file in upload query", HttpStatus.BAD_REQUEST);
        }

        // Get the file and save it somewhere
        byte[] bytes = file.getBytes();
        Path uploadFolder = applicationProperties.getUpload().getFolder();
        Files.createDirectories(uploadFolder);
        Path path = Paths.get(uploadFolder.toString(), file.getOriginalFilename());
        Files.write(path, bytes);
        log.debug("File uploaded successfully to {}", path);

        // Save document reference in database
        ProfileDocumentDTO profileDocument = new ProfileDocumentDTO();
        profileDocument.setName(file.getOriginalFilename());
        profileDocument.setPath(path.toString());
        profileDocumentService.save(profileDocument);

        log.debug("File persisted in database {}");
        return new ResponseEntity<>("Upload successful", HttpStatus.OK);
    }

}
