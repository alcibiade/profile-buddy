package org.alcibiade.hr.buddy.service.mapper;

import org.alcibiade.hr.buddy.domain.Profile;
import org.alcibiade.hr.buddy.domain.ProfileDocument;
import org.alcibiade.hr.buddy.service.dto.ProfileDocumentDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

/**
 * Mapper for the entity ProfileDocument and its DTO ProfileDocumentDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProfileDocumentMapper {

    @Mapping(source = "profile.id", target = "profileId")
    ProfileDocumentDTO profileDocumentToProfileDocumentDTO(ProfileDocument profileDocument);

    List<ProfileDocumentDTO> profileDocumentsToProfileDocumentDTOs(List<ProfileDocument> profileDocuments);

    @Mapping(source = "profileId", target = "profile")
    ProfileDocument profileDocumentDTOToProfileDocument(ProfileDocumentDTO profileDocumentDTO);

    List<ProfileDocument> profileDocumentDTOsToProfileDocuments(List<ProfileDocumentDTO> profileDocumentDTOs);

    default Profile profileFromId(Long id) {
        if (id == null) {
            return null;
        }
        Profile profile = new Profile();
        profile.setId(id);
        return profile;
    }
}
