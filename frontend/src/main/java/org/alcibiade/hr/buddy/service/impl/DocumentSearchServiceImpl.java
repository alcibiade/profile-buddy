package org.alcibiade.hr.buddy.service.impl;

import org.alcibiade.hr.buddy.domain.DocumentSearchResult;
import org.alcibiade.hr.buddy.repository.CrawlerRepository;
import org.alcibiade.hr.buddy.repository.ProfileDocumentRepository;
import org.alcibiade.hr.buddy.service.DocumentSearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Search implementation.
 */
@Component
public class DocumentSearchServiceImpl implements DocumentSearchService {

    private final Logger log = LoggerFactory.getLogger(DocumentSearchServiceImpl.class);
    private final CrawlerRepository crawlerRepository;
    private final ProfileDocumentRepository documentRepository;

    @Autowired
    public DocumentSearchServiceImpl(CrawlerRepository crawlerRepository, ProfileDocumentRepository documentRepository) {
        this.crawlerRepository = crawlerRepository;
        this.documentRepository = documentRepository;
    }

    @PostConstruct
    public void init() {
        log.info("Crawler index has {} nodes", this.crawlerRepository.count());
    }

    @Override
    public DocumentSearchResult search(String term) {
        List<DocumentSearchResult.Document> documents = crawlerRepository.findByContent(term).map(crawlerDocument -> {
            DocumentSearchResult.Document doc = new DocumentSearchResult.Document();
            doc.setFileName(crawlerDocument.getFile().getFilename());
            doc.setContent(crawlerDocument.getContent());
            doc.setContentType(crawlerDocument.getFile().getContent_type());
            doc.setFileSize(crawlerDocument.getFile().getFilesize());
            doc.setLastModified(crawlerDocument.getFile().getLast_modified());
            return doc;
        }).collect(Collectors.toList());

        DocumentSearchResult documentSearchResult = new DocumentSearchResult();
        documentSearchResult.setDocuments(documents);

        return documentSearchResult;
    }
}
