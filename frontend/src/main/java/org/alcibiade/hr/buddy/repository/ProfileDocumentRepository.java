package org.alcibiade.hr.buddy.repository;

import org.alcibiade.hr.buddy.domain.ProfileDocument;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the ProfileDocument entity.
 */
@SuppressWarnings("unused")
public interface ProfileDocumentRepository extends JpaRepository<ProfileDocument, Long> {

}
