package org.alcibiade.hr.buddy.repository;

import org.alcibiade.hr.buddy.domain.CrawlerDocument;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.stream.Stream;

/**
 * Crawler data access.
 */
public interface CrawlerRepository extends ElasticsearchRepository<CrawlerDocument, String> {
    Stream<CrawlerDocument> findByContent(String name);
}
