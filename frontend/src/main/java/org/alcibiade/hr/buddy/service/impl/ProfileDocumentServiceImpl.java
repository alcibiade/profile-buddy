package org.alcibiade.hr.buddy.service.impl;

import org.alcibiade.hr.buddy.domain.ProfileDocument;
import org.alcibiade.hr.buddy.repository.ProfileDocumentRepository;
import org.alcibiade.hr.buddy.service.ProfileDocumentService;
import org.alcibiade.hr.buddy.service.dto.ProfileDocumentDTO;
import org.alcibiade.hr.buddy.service.mapper.ProfileDocumentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing ProfileDocument.
 */
@Service
@Transactional
public class ProfileDocumentServiceImpl implements ProfileDocumentService {

    private final Logger log = LoggerFactory.getLogger(ProfileDocumentServiceImpl.class);

    private final ProfileDocumentRepository profileDocumentRepository;

    private final ProfileDocumentMapper profileDocumentMapper;

    public ProfileDocumentServiceImpl(ProfileDocumentRepository profileDocumentRepository, ProfileDocumentMapper profileDocumentMapper) {
        this.profileDocumentRepository = profileDocumentRepository;
        this.profileDocumentMapper = profileDocumentMapper;
    }

    /**
     * Save a profileDocument.
     *
     * @param profileDocumentDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ProfileDocumentDTO save(ProfileDocumentDTO profileDocumentDTO) {
        log.debug("Request to save ProfileDocument : {}", profileDocumentDTO);
        ProfileDocument profileDocument = profileDocumentMapper.profileDocumentDTOToProfileDocument(profileDocumentDTO);
        profileDocument = profileDocumentRepository.save(profileDocument);
        ProfileDocumentDTO result = profileDocumentMapper.profileDocumentToProfileDocumentDTO(profileDocument);
        return result;
    }

    /**
     * Get all the profileDocuments.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<ProfileDocumentDTO> findAll() {
        log.debug("Request to get all ProfileDocuments");
        List<ProfileDocumentDTO> result = profileDocumentRepository.findAll().stream()
            .map(profileDocumentMapper::profileDocumentToProfileDocumentDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }

    /**
     * Get one profileDocument by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ProfileDocumentDTO findOne(Long id) {
        log.debug("Request to get ProfileDocument : {}", id);
        ProfileDocument profileDocument = profileDocumentRepository.findOne(id);
        ProfileDocumentDTO profileDocumentDTO = profileDocumentMapper.profileDocumentToProfileDocumentDTO(profileDocument);
        return profileDocumentDTO;
    }

    /**
     * Delete the  profileDocument by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProfileDocument : {}", id);
        profileDocumentRepository.delete(id);
    }
}
