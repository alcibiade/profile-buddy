package org.alcibiade.hr.buddy.repository;

import org.alcibiade.hr.buddy.domain.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Profile entity.
 */
@SuppressWarnings("unused")
public interface ProfileRepository extends JpaRepository<Profile,Long> {

}
