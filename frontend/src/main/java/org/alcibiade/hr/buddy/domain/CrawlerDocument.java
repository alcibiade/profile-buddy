package org.alcibiade.hr.buddy.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * File index entry.
 */
@Document(indexName = "crawl", type = "doc")
public class CrawlerDocument {

    @Id
    private String id;

    private String content;

    @Field(type = FieldType.Object)
    private CrawlerFile file;

    public String getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public CrawlerFile getFile() {
        return file;
    }
}
