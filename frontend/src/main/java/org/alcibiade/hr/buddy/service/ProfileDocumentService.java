package org.alcibiade.hr.buddy.service;

import org.alcibiade.hr.buddy.service.dto.ProfileDocumentDTO;

import java.util.List;

/**
 * Service Interface for managing ProfileDocument.
 */
public interface ProfileDocumentService {

    /**
     * Save a profileDocument.
     *
     * @param profileDocumentDTO the entity to save
     * @return the persisted entity
     */
    ProfileDocumentDTO save(ProfileDocumentDTO profileDocumentDTO);

    /**
     * Get all the profileDocuments.
     *
     * @return the list of entities
     */
    List<ProfileDocumentDTO> findAll();

    /**
     * Get the "id" profileDocument.
     *
     * @param id the id of the entity
     * @return the entity
     */
    ProfileDocumentDTO findOne(Long id);

    /**
     * Delete the "id" profileDocument.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
