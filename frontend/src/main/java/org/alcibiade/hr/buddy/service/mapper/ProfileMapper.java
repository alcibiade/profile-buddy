package org.alcibiade.hr.buddy.service.mapper;

import org.alcibiade.hr.buddy.domain.Profile;
import org.alcibiade.hr.buddy.service.dto.ProfileDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

/**
 * Mapper for the entity Profile and its DTO ProfileDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProfileMapper {

    ProfileDTO profileToProfileDTO(Profile profile);

    List<ProfileDTO> profilesToProfileDTOs(List<Profile> profiles);

    @Mapping(target = "documents", ignore = true)
    Profile profileDTOToProfile(ProfileDTO profileDTO);

    List<Profile> profileDTOsToProfiles(List<ProfileDTO> profileDTOs);
}
