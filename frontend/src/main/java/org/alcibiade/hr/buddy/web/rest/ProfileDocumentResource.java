package org.alcibiade.hr.buddy.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import org.alcibiade.hr.buddy.service.ProfileDocumentService;
import org.alcibiade.hr.buddy.service.dto.ProfileDocumentDTO;
import org.alcibiade.hr.buddy.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ProfileDocument.
 */
@RestController
@RequestMapping("/api")
public class ProfileDocumentResource {

    private static final String ENTITY_NAME = "profileDocument";
    private final Logger log = LoggerFactory.getLogger(ProfileDocumentResource.class);
    private final ProfileDocumentService profileDocumentService;

    public ProfileDocumentResource(ProfileDocumentService profileDocumentService) {
        this.profileDocumentService = profileDocumentService;
    }

    /**
     * POST  /profile-documents : Create a new profileDocument.
     *
     * @param profileDocumentDTO the profileDocumentDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new profileDocumentDTO, or with status 400 (Bad Request) if the profileDocument has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/profile-documents")
    @Timed
    public ResponseEntity<ProfileDocumentDTO> createProfileDocument(@Valid @RequestBody ProfileDocumentDTO profileDocumentDTO) throws URISyntaxException {
        log.debug("REST request to save ProfileDocument : {}", profileDocumentDTO);
        if (profileDocumentDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new profileDocument cannot already have an ID")).body(null);
        }
        ProfileDocumentDTO result = profileDocumentService.save(profileDocumentDTO);
        return ResponseEntity.created(new URI("/api/profile-documents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /profile-documents : Updates an existing profileDocument.
     *
     * @param profileDocumentDTO the profileDocumentDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated profileDocumentDTO,
     * or with status 400 (Bad Request) if the profileDocumentDTO is not valid,
     * or with status 500 (Internal Server Error) if the profileDocumentDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/profile-documents")
    @Timed
    public ResponseEntity<ProfileDocumentDTO> updateProfileDocument(@Valid @RequestBody ProfileDocumentDTO profileDocumentDTO) throws URISyntaxException {
        log.debug("REST request to update ProfileDocument : {}", profileDocumentDTO);
        if (profileDocumentDTO.getId() == null) {
            return createProfileDocument(profileDocumentDTO);
        }
        ProfileDocumentDTO result = profileDocumentService.save(profileDocumentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, profileDocumentDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /profile-documents : get all the profileDocuments.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of profileDocuments in body
     */
    @GetMapping("/profile-documents")
    @Timed
    public List<ProfileDocumentDTO> getAllProfileDocuments() {
        log.debug("REST request to get all ProfileDocuments");
        return profileDocumentService.findAll();
    }

    /**
     * GET  /profile-documents/:id : get the "id" profileDocument.
     *
     * @param id the id of the profileDocumentDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the profileDocumentDTO, or with status 404 (Not Found)
     */
    @GetMapping("/profile-documents/{id}")
    @Timed
    public ResponseEntity<ProfileDocumentDTO> getProfileDocument(@PathVariable Long id) {
        log.debug("REST request to get ProfileDocument : {}", id);
        ProfileDocumentDTO profileDocumentDTO = profileDocumentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(profileDocumentDTO));
    }

    /**
     * DELETE  /profile-documents/:id : delete the "id" profileDocument.
     *
     * @param id the id of the profileDocumentDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/profile-documents/{id}")
    @Timed
    public ResponseEntity<Void> deleteProfileDocument(@PathVariable Long id) {
        log.debug("REST request to delete ProfileDocument : {}", id);
        profileDocumentService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
