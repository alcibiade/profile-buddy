package org.alcibiade.hr.buddy.service.impl;

import org.alcibiade.hr.buddy.domain.Profile;
import org.alcibiade.hr.buddy.repository.ProfileRepository;
import org.alcibiade.hr.buddy.service.ProfileService;
import org.alcibiade.hr.buddy.service.dto.ProfileDTO;
import org.alcibiade.hr.buddy.service.mapper.ProfileMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Profile.
 */
@Service
@Transactional
public class ProfileServiceImpl implements ProfileService{

    private final Logger log = LoggerFactory.getLogger(ProfileServiceImpl.class);

    private final ProfileRepository profileRepository;

    private final ProfileMapper profileMapper;

    public ProfileServiceImpl(ProfileRepository profileRepository, ProfileMapper profileMapper) {
        this.profileRepository = profileRepository;
        this.profileMapper = profileMapper;
    }

    /**
     * Save a profile.
     *
     * @param profileDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ProfileDTO save(ProfileDTO profileDTO) {
        log.debug("Request to save Profile : {}", profileDTO);
        Profile profile = profileMapper.profileDTOToProfile(profileDTO);
        profile = profileRepository.save(profile);
        ProfileDTO result = profileMapper.profileToProfileDTO(profile);
        return result;
    }

    /**
     *  Get all the profiles.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<ProfileDTO> findAll() {
        log.debug("Request to get all Profiles");
        List<ProfileDTO> result = profileRepository.findAll().stream()
            .map(profileMapper::profileToProfileDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }

    /**
     *  Get one profile by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ProfileDTO findOne(Long id) {
        log.debug("Request to get Profile : {}", id);
        Profile profile = profileRepository.findOne(id);
        ProfileDTO profileDTO = profileMapper.profileToProfileDTO(profile);
        return profileDTO;
    }

    /**
     *  Delete the  profile by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Profile : {}", id);
        profileRepository.delete(id);
    }
}
