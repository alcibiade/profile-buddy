package org.alcibiade.hr.buddy.service.dto;


import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ProfileDocument entity.
 */
public class ProfileDocumentDTO implements Serializable {

    private Long id;

    @NotNull
    private String path;

    @NotNull
    private String name;

    private Long profileId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProfileDocumentDTO profileDocumentDTO = (ProfileDocumentDTO) o;

        return Objects.equals(id, profileDocumentDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ProfileDocumentDTO{" +
            "id=" + id +
            ", path='" + path + "'" +
            ", name='" + name + "'" +
            '}';
    }
}
