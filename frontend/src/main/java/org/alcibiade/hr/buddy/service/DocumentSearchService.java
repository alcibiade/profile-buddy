package org.alcibiade.hr.buddy.service;

import org.alcibiade.hr.buddy.domain.DocumentSearchResult;

/**
 * Service Interface for managing ProfileDocument.
 */
public interface DocumentSearchService {

    DocumentSearchResult search(String term);
}
