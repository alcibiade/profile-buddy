package org.alcibiade.hr.buddy.repository;

import org.alcibiade.hr.buddy.domain.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Comment entity.
 */
@SuppressWarnings("unused")
public interface CommentRepository extends JpaRepository<Comment,Long> {

}
