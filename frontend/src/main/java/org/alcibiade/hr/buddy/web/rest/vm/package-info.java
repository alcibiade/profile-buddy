/**
 * View Models used by Spring MVC REST controllers.
 */
package org.alcibiade.hr.buddy.web.rest.vm;
