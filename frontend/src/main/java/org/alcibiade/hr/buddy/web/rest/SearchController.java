package org.alcibiade.hr.buddy.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.alcibiade.hr.buddy.config.ApplicationProperties;
import org.alcibiade.hr.buddy.domain.DocumentSearchResult;
import org.alcibiade.hr.buddy.service.DocumentSearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Document search API
 */
@RestController
@RequestMapping("/api")
public class SearchController {
    private final Logger log = LoggerFactory.getLogger(SearchController.class);
    private final ApplicationProperties applicationProperties;

    private DocumentSearchService documentSearchService;

    @Autowired
    public SearchController(ApplicationProperties applicationProperties,
                            DocumentSearchService documentSearchService) {
        this.applicationProperties = applicationProperties;
        this.documentSearchService = documentSearchService;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @Timed
    public DocumentSearchResult search(String term) {
        log.debug("Searching for term {}", term);
        return documentSearchService.search(term);
    }

}
