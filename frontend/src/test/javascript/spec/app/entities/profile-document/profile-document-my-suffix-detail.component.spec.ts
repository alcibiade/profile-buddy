import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MockBackend} from '@angular/http/testing';
import {BaseRequestOptions, Http} from '@angular/http';
import {OnInit} from '@angular/core';
import {DatePipe} from '@angular/common';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {DataUtils, DateUtils, JhiLanguageService} from 'ng-jhipster';
import {MockLanguageService} from '../../../helpers/mock-language.service';
import {MockActivatedRoute} from '../../../helpers/mock-route.service';
import {ProfileDocumentMySuffixDetailComponent} from '../../../../../../main/webapp/app/entities/profile-document/profile-document-my-suffix-detail.component';
import {ProfileDocumentMySuffixService} from '../../../../../../main/webapp/app/entities/profile-document/profile-document-my-suffix.service';
import {ProfileDocumentMySuffix} from '../../../../../../main/webapp/app/entities/profile-document/profile-document-my-suffix.model';

describe('Component Tests', () => {

    describe('ProfileDocumentMySuffix Management Detail Component', () => {
        let comp: ProfileDocumentMySuffixDetailComponent;
        let fixture: ComponentFixture<ProfileDocumentMySuffixDetailComponent>;
        let service: ProfileDocumentMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [ProfileDocumentMySuffixDetailComponent],
                providers: [
                    MockBackend,
                    BaseRequestOptions,
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    {
                        provide: Http,
                        useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => {
                            return new Http(backendInstance, defaultOptions);
                        },
                        deps: [MockBackend, BaseRequestOptions]
                    },
                    {
                        provide: JhiLanguageService,
                        useClass: MockLanguageService
                    },
                    ProfileDocumentMySuffixService
                ]
            }).overrideComponent(ProfileDocumentMySuffixDetailComponent, {
                set: {
                    template: ''
                }
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProfileDocumentMySuffixDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProfileDocumentMySuffixService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new ProfileDocumentMySuffix(10)));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.profileDocument).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
