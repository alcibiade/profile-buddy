import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MockBackend} from '@angular/http/testing';
import {BaseRequestOptions, Http} from '@angular/http';
import {OnInit} from '@angular/core';
import {DatePipe} from '@angular/common';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {DataUtils, DateUtils, JhiLanguageService} from 'ng-jhipster';
import {MockLanguageService} from '../../../helpers/mock-language.service';
import {MockActivatedRoute} from '../../../helpers/mock-route.service';
import {ProfileMySuffixDetailComponent} from '../../../../../../main/webapp/app/entities/profile/profile-my-suffix-detail.component';
import {ProfileMySuffixService} from '../../../../../../main/webapp/app/entities/profile/profile-my-suffix.service';
import {ProfileMySuffix} from '../../../../../../main/webapp/app/entities/profile/profile-my-suffix.model';

describe('Component Tests', () => {

    describe('ProfileMySuffix Management Detail Component', () => {
        let comp: ProfileMySuffixDetailComponent;
        let fixture: ComponentFixture<ProfileMySuffixDetailComponent>;
        let service: ProfileMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [ProfileMySuffixDetailComponent],
                providers: [
                    MockBackend,
                    BaseRequestOptions,
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    {
                        provide: Http,
                        useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => {
                            return new Http(backendInstance, defaultOptions);
                        },
                        deps: [MockBackend, BaseRequestOptions]
                    },
                    {
                        provide: JhiLanguageService,
                        useClass: MockLanguageService
                    },
                    ProfileMySuffixService
                ]
            }).overrideComponent(ProfileMySuffixDetailComponent, {
                set: {
                    template: ''
                }
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProfileMySuffixDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProfileMySuffixService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ProfileMySuffix(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.profile).toEqual(jasmine.objectContaining({id:10}));
            });
        });
    });

});
