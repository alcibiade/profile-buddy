import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MockBackend} from '@angular/http/testing';
import {BaseRequestOptions, Http} from '@angular/http';
import {OnInit} from '@angular/core';
import {DatePipe} from '@angular/common';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {DataUtils, DateUtils, JhiLanguageService} from 'ng-jhipster';
import {MockLanguageService} from '../../../helpers/mock-language.service';
import {MockActivatedRoute} from '../../../helpers/mock-route.service';
import {CommentMySuffixDetailComponent} from '../../../../../../main/webapp/app/entities/comment/comment-my-suffix-detail.component';
import {CommentMySuffixService} from '../../../../../../main/webapp/app/entities/comment/comment-my-suffix.service';
import {CommentMySuffix} from '../../../../../../main/webapp/app/entities/comment/comment-my-suffix.model';

describe('Component Tests', () => {

    describe('CommentMySuffix Management Detail Component', () => {
        let comp: CommentMySuffixDetailComponent;
        let fixture: ComponentFixture<CommentMySuffixDetailComponent>;
        let service: CommentMySuffixService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [CommentMySuffixDetailComponent],
                providers: [
                    MockBackend,
                    BaseRequestOptions,
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    {
                        provide: Http,
                        useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => {
                            return new Http(backendInstance, defaultOptions);
                        },
                        deps: [MockBackend, BaseRequestOptions]
                    },
                    {
                        provide: JhiLanguageService,
                        useClass: MockLanguageService
                    },
                    CommentMySuffixService
                ]
            }).overrideComponent(CommentMySuffixDetailComponent, {
                set: {
                    template: ''
                }
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CommentMySuffixDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CommentMySuffixService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new CommentMySuffix(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.comment).toEqual(jasmine.objectContaining({id:10}));
            });
        });
    });

});
