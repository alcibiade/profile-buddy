package org.alcibiade.hr.buddy.web.rest;

import org.alcibiade.hr.buddy.ProfileBuddyApp;
import org.alcibiade.hr.buddy.domain.ProfileDocument;
import org.alcibiade.hr.buddy.repository.ProfileDocumentRepository;
import org.alcibiade.hr.buddy.service.ProfileDocumentService;
import org.alcibiade.hr.buddy.service.dto.ProfileDocumentDTO;
import org.alcibiade.hr.buddy.service.mapper.ProfileDocumentMapper;
import org.alcibiade.hr.buddy.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ProfileDocumentResource REST controller.
 *
 * @see ProfileDocumentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProfileBuddyApp.class)
public class ProfileDocumentResourceIntTest {

    private static final String DEFAULT_PATH = "AAAAAAAAAA";
    private static final String UPDATED_PATH = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private ProfileDocumentRepository profileDocumentRepository;

    @Autowired
    private ProfileDocumentMapper profileDocumentMapper;

    @Autowired
    private ProfileDocumentService profileDocumentService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restProfileDocumentMockMvc;

    private ProfileDocument profileDocument;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProfileDocument createEntity(EntityManager em) {
        ProfileDocument profileDocument = new ProfileDocument()
            .path(DEFAULT_PATH)
            .name(DEFAULT_NAME);
        return profileDocument;
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ProfileDocumentResource profileDocumentResource = new ProfileDocumentResource(profileDocumentService);
        this.restProfileDocumentMockMvc = MockMvcBuilders.standaloneSetup(profileDocumentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        profileDocument = createEntity(em);
    }

    @Test
    @Transactional
    public void createProfileDocument() throws Exception {
        int databaseSizeBeforeCreate = profileDocumentRepository.findAll().size();

        // Create the ProfileDocument
        ProfileDocumentDTO profileDocumentDTO = profileDocumentMapper.profileDocumentToProfileDocumentDTO(profileDocument);
        restProfileDocumentMockMvc.perform(post("/api/profile-documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(profileDocumentDTO)))
            .andExpect(status().isCreated());

        // Validate the ProfileDocument in the database
        List<ProfileDocument> profileDocumentList = profileDocumentRepository.findAll();
        assertThat(profileDocumentList).hasSize(databaseSizeBeforeCreate + 1);
        ProfileDocument testProfileDocument = profileDocumentList.get(profileDocumentList.size() - 1);
        assertThat(testProfileDocument.getPath()).isEqualTo(DEFAULT_PATH);
        assertThat(testProfileDocument.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createProfileDocumentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = profileDocumentRepository.findAll().size();

        // Create the ProfileDocument with an existing ID
        profileDocument.setId(1L);
        ProfileDocumentDTO profileDocumentDTO = profileDocumentMapper.profileDocumentToProfileDocumentDTO(profileDocument);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProfileDocumentMockMvc.perform(post("/api/profile-documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(profileDocumentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<ProfileDocument> profileDocumentList = profileDocumentRepository.findAll();
        assertThat(profileDocumentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkPathIsRequired() throws Exception {
        int databaseSizeBeforeTest = profileDocumentRepository.findAll().size();
        // set the field null
        profileDocument.setPath(null);

        // Create the ProfileDocument, which fails.
        ProfileDocumentDTO profileDocumentDTO = profileDocumentMapper.profileDocumentToProfileDocumentDTO(profileDocument);

        restProfileDocumentMockMvc.perform(post("/api/profile-documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(profileDocumentDTO)))
            .andExpect(status().isBadRequest());

        List<ProfileDocument> profileDocumentList = profileDocumentRepository.findAll();
        assertThat(profileDocumentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = profileDocumentRepository.findAll().size();
        // set the field null
        profileDocument.setName(null);

        // Create the ProfileDocument, which fails.
        ProfileDocumentDTO profileDocumentDTO = profileDocumentMapper.profileDocumentToProfileDocumentDTO(profileDocument);

        restProfileDocumentMockMvc.perform(post("/api/profile-documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(profileDocumentDTO)))
            .andExpect(status().isBadRequest());

        List<ProfileDocument> profileDocumentList = profileDocumentRepository.findAll();
        assertThat(profileDocumentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProfileDocuments() throws Exception {
        // Initialize the database
        profileDocumentRepository.saveAndFlush(profileDocument);

        // Get all the profileDocumentList
        restProfileDocumentMockMvc.perform(get("/api/profile-documents?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(profileDocument.getId().intValue())))
            .andExpect(jsonPath("$.[*].path").value(hasItem(DEFAULT_PATH.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getProfileDocument() throws Exception {
        // Initialize the database
        profileDocumentRepository.saveAndFlush(profileDocument);

        // Get the profileDocument
        restProfileDocumentMockMvc.perform(get("/api/profile-documents/{id}", profileDocument.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(profileDocument.getId().intValue()))
            .andExpect(jsonPath("$.path").value(DEFAULT_PATH.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingProfileDocument() throws Exception {
        // Get the profileDocument
        restProfileDocumentMockMvc.perform(get("/api/profile-documents/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProfileDocument() throws Exception {
        // Initialize the database
        profileDocumentRepository.saveAndFlush(profileDocument);
        int databaseSizeBeforeUpdate = profileDocumentRepository.findAll().size();

        // Update the profileDocument
        ProfileDocument updatedProfileDocument = profileDocumentRepository.findOne(profileDocument.getId());
        updatedProfileDocument
            .path(UPDATED_PATH)
            .name(UPDATED_NAME);
        ProfileDocumentDTO profileDocumentDTO = profileDocumentMapper.profileDocumentToProfileDocumentDTO(updatedProfileDocument);

        restProfileDocumentMockMvc.perform(put("/api/profile-documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(profileDocumentDTO)))
            .andExpect(status().isOk());

        // Validate the ProfileDocument in the database
        List<ProfileDocument> profileDocumentList = profileDocumentRepository.findAll();
        assertThat(profileDocumentList).hasSize(databaseSizeBeforeUpdate);
        ProfileDocument testProfileDocument = profileDocumentList.get(profileDocumentList.size() - 1);
        assertThat(testProfileDocument.getPath()).isEqualTo(UPDATED_PATH);
        assertThat(testProfileDocument.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingProfileDocument() throws Exception {
        int databaseSizeBeforeUpdate = profileDocumentRepository.findAll().size();

        // Create the ProfileDocument
        ProfileDocumentDTO profileDocumentDTO = profileDocumentMapper.profileDocumentToProfileDocumentDTO(profileDocument);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restProfileDocumentMockMvc.perform(put("/api/profile-documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(profileDocumentDTO)))
            .andExpect(status().isCreated());

        // Validate the ProfileDocument in the database
        List<ProfileDocument> profileDocumentList = profileDocumentRepository.findAll();
        assertThat(profileDocumentList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteProfileDocument() throws Exception {
        // Initialize the database
        profileDocumentRepository.saveAndFlush(profileDocument);
        int databaseSizeBeforeDelete = profileDocumentRepository.findAll().size();

        // Get the profileDocument
        restProfileDocumentMockMvc.perform(delete("/api/profile-documents/{id}", profileDocument.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ProfileDocument> profileDocumentList = profileDocumentRepository.findAll();
        assertThat(profileDocumentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProfileDocument.class);
    }
}
